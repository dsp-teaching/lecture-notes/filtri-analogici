f=0.01:0.01:5;
order=4;

B=10*log10(analog_butter(order, 1, f));
C=10*log10(analog_cheby(order, 1, 1, f));

semilogx(f, B, f, C)

grid on
ylim([-80, 3])
line([1 1], ylim)
line(xlim, -3*[1 1])
text(2, -2.6, '3 dB', 'verticalalignment', 'bottom')
legend('Butterworth', 'Chebyshev', 'location', 'southwest')
xlabel('Frequency')
ylabel('dB')
title(sprintf('Order=%d', order));

font_grandi

print('-dpdfwrite',  ...
      sprintf('butter_vs_cheby-order=%d.pdf', order));

xlim([0.01 2])
ylim([-10 5])

print('-dpdfwrite',  ...
      sprintf('butter_vs_cheby_zoom_in_passband-order=%d.pdf', order));

