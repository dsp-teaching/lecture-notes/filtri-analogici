order=4:6;

figure(1)
clf

figure(2)
clf

f=0.01:0.01:100;

labels={};
for N=order
  H=analog_cheby(N, 1, 1, f);
  H = 1-H;

  figure(1)
  semilogx(1 ./ f, 10*log10(H));
  hold on

  figure(2)
  semilogx(1 ./ f, H);
  hold on  

  labels{end+1}=sprintf('Order=%d', N);
endfor


figure(1)
ylim([-30 5])
line([1 1], ylim, 'linestyle', ':', 'color', 'black')
line(xlim, -3*[1 1], 'linestyle', ':', 'color', 'black')
text(0.5, -2.7, '3 dB', 'verticalalignment', 'bottom')
grid on

legend(labels, 'location', 'southwest')
xlabel('Frequency')
ylabel('dB')

font_grandi

print -dpdfwrite esempi-chebi2.pdf

figure(2)
ylim([0 1.1])
line([1 1], ylim, 'linestyle', ':', 'color', 'black')
line(xlim, 0.5*[1 1], 'linestyle', ':', 'color', 'black')
text(0.5, 0.525, '3 dB', 'verticalalignment', 'bottom')
grid on

legend(labels, 'location', 'southwest')
xlabel('Frequency')
ylabel('|H(f)|^2')

font_grandi

print -dpdfwrite esempi-chebi2-linear.pdf
