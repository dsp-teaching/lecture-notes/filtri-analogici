#!/bin/bash

branch=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')

if [[ "$branch" =~ ^V[0-9][0-9]\.[0-9][0-9][0-9]-[-0-9]* ]]; then
    echo "$branch" | tee version.txt
else
    echo "Branch '$branch' does not like a version branch"
    exit 1
fi

pdflatex lecture-notes.tex

mv lecture-notes.pdf "$branch-lecture-notes-on-analog-filters.pdf"



