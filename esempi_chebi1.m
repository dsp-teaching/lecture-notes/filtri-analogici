ordine=4:6;
cebi=@(N, x) cos(N*acos(x));
do_print = [2];
epsilon=1;

for k=1:3
  figure(k)
  clf
end


x=-1.5:0.005:1.5;
labels={};
stili = {'-b', '--r', '-.k'};
for N=ordine
  y=cebi(N, x);

  figure(1)
  plot(x, y, stili{1})
  hold on

  figure(2)
  plot(x, y.^2, stili{1})
  hold on

  figure(3)
  f=0.01:0.01:2;
  T = cebi(N, f);
  H = 1 ./ (1 + epsilon^2 * T.^2);
  plot(f, 10*log10(H), stili{1});
  hold on

  stili(1)=[];
  
  labels{end+1}=sprintf('N=%d', N);
endfor

for k=1:2
  figure(k)
  xlabel('x');

  if k==2
    name='polinomi_cheby_quadro.pdf';
    square_maybe='^2';
    ylim([-0.5, 3])
    xlim(1.2*[-1, 1])
  else
    name='polinomi_cheby.pdf';
    square_maybe='';
    ylim([-3, 3])
  end

  ylabel(['T_N' square_maybe '(x)'])
  grid on
  legend(labels, 'location', 'north')
  font_grandi

  if any(do_print < 0) || any(do_print == 1)
    print('-dpdfwrite', name);
  end
endfor

figure(3)
ylim([-30 1])
xlim([0.01 2])
line([1 1], ylim, 'linestyle', ':', 'color', 'k')
line(xlim, -3*[1 1], 'linestyle', ':', 'color', 'k')
text(1.5, -2.7, '3 dB', 'verticalalignment', 'bottom')
grid on
legend(labels, 'location', 'southwest')
xlabel('f')
ylabel('dB')
font_grandi

if any(do_print < 0) || any(do_print == 2)
  print('-dpdfwrite', 'filtri_chebyshev.pdf');
end
