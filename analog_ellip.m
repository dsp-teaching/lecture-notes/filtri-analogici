function y=analog_ellip(order, xi, epsilon, f)
  R = rational_ellip(order, xi, f);

  y = 1 ./ (1 + epsilon^2*R.^2);
endfunction
