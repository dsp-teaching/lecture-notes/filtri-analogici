function H=analog_cheby(order, fc, epsilon, f)
  T = cos(order * acos(f / fc));
  H = 1 ./ (1 + epsilon^2 * T.^2);
endfunction
