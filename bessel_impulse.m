function h=bessel_impulse(order, t)
  [z, poles, gain]=besselap(order);

  if ~isempty(z)
    error('Something wrong')
  end

  poles=poles(:);
  t = t(:)';

  h=zeros(size(t));

  for k=1:length(poles)
    q=poles;
    q(k)=[];
    Ak = 1 / prod(poles(k)-q);
    
    h = h + Ak*exp(t*poles(k));
  end

  if norm(imag(h)) > 1e-6
    warning('Imag=%f', norm(imag(h)))
  endif

  h=real(h);
endfunction
