#!/bin/bash

function is_git_status_dirty() {
    status=$(git status -s)
    [ -n "$status" ]
}

function latest_branch_release_number() {
    pattern='origin/V[0-9][0-9].[0-9][0-9][0-9]*'
    
    last_release_branch=$(git branch -r --list "$pattern"  \
			      | sort \
			      | tail -1)

    rel_number_regexp='s/origin.V[0-9]\{2\}\.\([0-9]\{3\}\)-.*$/\1/p'

    rel_number=$(echo $last_release_branch \
		     | sed -n -e "$rel_number_regexp")

    #
    # Remove any initial zeros from $rel_number, otherwise it
    # is interpreted as an octal number
    #
    echo $rel_number | sed -e 's/^0*\([^0].*\)$/\1/'
}

if is_git_status_dirty ;  then
    echo "CURRENT TREE IS NOT CLEAN. Exiting"
    echo ---------------------------
    echo
    
    git status

    exit 1 
fi

if [ $# -eq 0 ] ; then
    let new_release=`latest_branch_release_number`+1

elif  [ $# -eq 1 ] ; then
    new_release=$1

else
    echo "Usage: $0 [new release number]"
    exit 1
fi

  
new_branch=$(printf 'V22.%03d-%s' $new_release `date +%Y-%m-%d`)

git checkout -b $new_branch
    

