function q=butterworth_example(fp, fs, rp, rs)
  K = ((1/rs^2) - 1) / ((1/rp^2)-1)

  num = log10(K)

  ratio=fs/fp

  decades = log10(ratio)

  preN = 0.5 * num/decades

  N = ceil(preN)

  fc_min = fp / ((1/rp^2)-1)^(1/(2*N))
  fc_max = fs / ((1/rs^2)-1)^(1/(2*N))

  f=0:2*fs;
  H=@(f, N, fc)  10*log10(1 ./ (1 + (f ./ fc) .^ (2*N)))
  label=@(x) sprintf('f_c=%6.1f Hz', x);
  
  for k=1:2
    figure(k)
    clf

    plot(f, H(f, N, fc_min), ...
	 f, H(f, N, fc_max));

    
    rp_db=20*log10(rp);
    rs_db=20*log10(rs);

    ylim([rs_db-10 0])

    db_min = min(ylim);
    fmax = max(xlim);

    make_area(0, fp, 0, rp_db);
    make_area(fs, fmax, db_min, rs_db);

    if 0
      line([0 fp fp 0 0], ...
	   [0 0 rp_db rp_db 0], ...
	   'color', 'black')
      
      line([fs fs fmax fmax fs], ...
	   [db_min rs_db rs_db db_min db_min], ...
	 'color', 'black')

    q=patch([fs fs fmax fmax fs], ...
	 [db_min rs_db rs_db db_min db_min], ...
	 'blue')

    set(q, 'facealpha', 0.1)
    end
    
    legend(label(fc_min), label(fc_max));
    xlabel('Hz')
    ylabel('dB')
    grid on
    
    if k==2
      xlim([0 (fp+fs)/2])
      20*log10(rp)
      ylim([20*log10(rp)-1, 0])
    endif

    font_grandi
  endfor
  
endfunction

function make_area(x1, x2, y1, y2)
  x=[x1 x2 x2 x1 x1];
  y=[y1 y1 y2 y2 y1];
  line(x, y, 'color', 'black')
  h=patch(x, y, 'blue')
  set(h, 'facealpha', 0.15)
endfunction
