stampa = 1;

% Progetta un filtro a fase lineare
filtro_fase_lin=firpm(50, [0, 0.1, 0.3, 1], [1, 1, 0, 0]);

% Trasforma il filtro a fase lineare in un filtro a fase minima
% sostituendo ogni zero in modulo > 1 con il suo inverso

zeri = roots(filtro_fase_lin);
idx = find(abs(zeri) > 1);
zeri(idx) = 1 ./ zeri(idx);

% Mappa gli zeri nel filtro
filtro_fase_min = poly(zeri);
filtro_fase_min = filtro_fase_min / sum(filtro_fase_min);

x = [zeros(1, 20), ones(1, 40), zeros(1, 20)];

y_fase_lin = conv(x, filtro_fase_lin);
y_fase_min = conv(x, filtro_fase_min);

figure(1)
clf
plot(filtro_fase_lin, 'r')
hold on
plot(filtro_fase_min, 'b--')
legend('Fase lineare', 'Fase minima')
title('Confronto tra risposte impulsive')
if stampa 
  print -depsc faselin_impulse.eps
end

figure(2)
clf
plot(x, 'k:');
hold on
plot(y_fase_lin, 'r')
plot(y_fase_min, 'b--')
legend('Input', 'Fase lineare', 'Fase minima')
title('Azione su un rect')
if stampa 
  print -depsc faselin_result.eps
end
