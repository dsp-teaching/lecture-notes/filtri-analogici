order=3:2:11;

f=0.001:0.001:5;

% function fc=find_fc(p, level)
%   fmin=0;
%   fmax=20;
% 
%   while fmax - fmin > 1e-3
%     fnew = (fmax+fmin)/2;
%     Anew = 20*log10(p(end) / abs(polyval(p, j*2*pi*fnew)));
% 
%     if abs(Anew-level) < 1e-6
%       fc=fnew;
%       return;
%     endif
%     
%     if Anew < level
%       fmax = fnew;
%     else
%       fmin = fnew;
%     endif
%   endwhile
% 
%   fc=fnew;
% endfunction

figure(1)
clf

labels={};
for N=order
  p = reverse_bessel(N);
  fc = bessel_3db_freq(N);

  A = p(end) ./ abs(polyval(p, j*2*pi*(f*fc)));

  semilogx(f, 20*log10(A));
  hold on

  labels{end+1}=sprintf('N=%d', N);
end

ylim([-30 1])
line([1 1], ylim)
line(xlim, -3*[1 1])
text(5e-3, -2.7, '3 dB', 'verticalalignment', 'bottom')

legend(labels, 'location', 'southwest')
grid on
xlabel('Frequency')
ylabel('dB')
font_grandi

print -dpdfwrite filtri_bessel.pdf

figure(2)
clf
figure(3)
clf

labels={};
t=0:0.01:20;

for N=order
  h = bessel_impulse(N, t);

  figure(2)
  plot(t, h)
  hold on

  figure(3)
  fc=bessel_3db_freq(N);
  plot(t ./ fc, h * fc);
  hold on

  labels{end+1}=sprintf('N=%d', N);
end

for k=2:3
  figure(k)
  legend(labels)
  grid on
  font_grandi
  xlabel('t')
  ylabel('h(t)')

  if k == 2
    print -dpdfwrite bessel_impulse.pdf
  else
    xlim([0 50])
    print -dpdfwrite bessel_impulse_rescaled.pdf

    ylim(0.01*[-1,1])
    xlim([20 50])
    print -dpdfwrite bessel_impulse_zoom_on_tail.pdf

  endif
end


