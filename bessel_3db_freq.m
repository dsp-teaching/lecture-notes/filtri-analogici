function f=bessel_3db_freq(order)
  known = [NaN
	   0.216674804687500
	   0.278930664062500
	   0.336303710937500
	   0.385131835937500
	   0.429077148437500
	   0.469360351562500
	   0.504760742187500
	   0.538940429687500
	   0.570678710937500
	   0.599975585937500
	   0.629272460937500
	   0.656127929687500
	   0.682983398437500
	   0.707397460937500
	  ];

  if 0 % order+1 <= length(known)
    f = known(order+1);
  else
    p = reverse_bessel(order);
    f = find_fc(p, -3);
  endif 
endfunction

function fc=find_fc(p, level)
  fmin=0;
  fmax=20;

  while fmax - fmin > 1e-3
    fnew = (fmax+fmin)/2;
    Anew = 20*log10(p(end) / abs(polyval(p, j*2*pi*fnew)));

    if abs(Anew-level) < 1e-6
      fc=fnew;
      return;
    endif
    
    if Anew < level
      fmax = fnew;
    else
      fmin = fnew;
    endif
  endwhile

  fc=fnew;
endfunction
