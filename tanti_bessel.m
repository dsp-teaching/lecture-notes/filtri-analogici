stili = {'b', 'r', 'g', 'k', 'b--', 'r--', 'g--', 'k--'};
freq_scale = 0.56*[1 0.8 0.7 0.65  0.6 0.55];
labels = {};

for n=1:length(freq_scale); 
    [z,p,k]=besselap(n); 
    [num,den] = zp2tf(z, p, k); 
    [H, f]=freqs(num,den); 
    
    semilogx(f/freq_scale(n), 20*log10(abs(H)), stili{n}); 
    labels{end+1} = sprintf('N=%d', n);
    hold on; 
end

line(xlim, -1*[1 1]);
line(xlim, -3*[1 1]); 
line(1.1*[1 1], ylim, 'color', 'b'); 
line(8*1.1*[1 1], ylim, 'color', 'r'); 
line(16*1.1*[1 1], ylim, 'color', 'g'); 

ylim([-50, 10]);

grid on;
legend(labels{:}, 'Location', 'SouthWest');
