function H=analog_butter(order, fc, f)
  H = 1 ./ (1 + (f ./ fc).^(2*order));
endfunction
