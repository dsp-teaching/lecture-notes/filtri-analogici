order = [1, 2, 3, 4];
do_print=[2 3];

x=-1:0.01:1;
xi=1.4;

figure(1)
clf

figure(2)
clf

labels={};
for N=order
  y=rational_ellip(N, xi, x);

  figure(1)
  plot(x, y);
  hold on

  figure(2)
  plot(x, y.^2);
  hold on

  labels{end+1}=sprintf('N=%d', N);
end

for k=1:2
  figure(k)
  xlabel('x')
  if k==1
    ylabel(sprintf('R_N(%3.1f, x)', xi));
  else
    ylabel(sprintf('R_N^2(%3.1f, x)', xi));
  endif

  legend(labels)
  
  grid on

  font_grandi

  if any(do_print==1)
    if k==1
      print -dpdfwrite ellip_function.pdf
    else
      print -dpdfwrite ellip_function_squared.pdf
    end
  end
end

figure(3)
clf

N=6;
xi=2;
epsilon=0.5;
f=0.01:0.001:12; 20;
H=analog_ellip(N, xi, epsilon, f);
[R,L]=rational_ellip(N, xi, f);

[h, l1, l2]=plotyy(f, (H), f, log10(abs(R)));

set(l2, 'linestyle', '--')
axes(h(2))
stile={'linestyle', ':', 'color', 'k'};
line(h(2),[1 1], ylim, stile{:})
line(h(2),xi*[1 1], ylim, stile{:})
line(h(2),xlim, [0 0], stile{:})
line(h(2),xlim, log10(abs(L))+[0 0], stile{:})
xlabel('Freq.')
ylabel(h(1), '|H(f)|^2')
ylabel(h(2), 'log_{10}(R_N)')

for k=1:2
  axes(h(k))
  font_grandi
end

if any(do_print == 2)
  print -dpdfwrite ellip_R_and_filter.pdf

  axes(h(1))
  ylim([0 5e-9])
  axes(h(2))

  print -dpdfwrite ellip_R_and_filter_zoom.pdf
end

%else
%  subplot(2,1,1)
%  semilogy(f, H);
%  grid on
%
%  subplot(2,1,2)
%  semilogy(f, abs(R));
%  grid on
%end


figure(4)
clf

x=0.1:0.001:10;
xi=1.4;
y = rational_ellip(4, xi, x);
t = sqrt(1-1/xi^2);
u = xi^2*(1-sqrt(t));
v = sqrt(t*(t+1));
zeri = [sqrt(u*(1+t-v)), sqrt(u*(1+t+v))];

loglog(x, abs(y));

stile={'linestyle', '--', 'color', 'k' };
for z=zeri
  line(z+[0 0], ylim, stile{:});
  line(xi/z+[0 0], ylim, stile{:});
end

line(sqrt(xi)+[0 0], ylim, 'linestyle', '-.', 'color', 'r')
xlabel('x')
ylabel('|R_{4,\xi}(x)|')
grid on
set(gca, 'yminorgrid', 'off')
title(['\xi=', sprintf('%3.1f', xi)])

font_grandi

if any(do_print == 3)
  print -dpdfwrite ellip_zeros_and_poles.pdf
endif
