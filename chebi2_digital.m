stampa=1;

fp=0.3;
if 0
  fs=0.37;
else
  fs=0.4;
end

rp=-20*log10(0.9);
rs=30;

[N, fc]=cheb2ord(fp, fs, rp, rs);
[num, den]=cheby2(N, rs, fc);

figure(1)
clf
[H, W]=freqz(num, den);
plot(W / (2*pi), abs(H))
xlabel('Norm. frequencies')
ylabel('|H(f)|')
title(sprintf('Digital Chebyshev-II order=%d', N))
font_grandi

if stampa
  print('-dpdfwrite', ...
	sprintf('chebi2_discrete-order=%d.pdf', N))
end

figure(2)
clf
zplane(num, den)
title(sprintf('Digital Chebyshev-II order=%d', N))

font_grandi

if stampa
  print('-dpdfwrite', ...
	sprintf('chebi2_discrete_zeri_e_poli-order=%d.pdf', N))
end
