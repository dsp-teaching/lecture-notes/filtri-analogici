rp=-20*log10(0.9);
ra=30;

fig_num = [1 2]
ordini = [4 6 8];

stili = {'b', 'r--', 'g-.'};

f=logspace(-1, 1, 1000);

for fig=fig_num
  figure(fig)
  hold off
end

for N=ordini
  [zeri, poli, cost] = ellipap(N, rp, ra);
  zeri = zeri(:)';
  poli = poli(:)';
  
  H = cost*ones(size(f));

  for z=zeri
    H = H.* (j*f - z);
  end

  for p=poli
    H = H./ (j*f - p);
  end

  for fig=fig_num
    figure(fig)
    loglog(f, abs(H), stili{1});
    hold on
  end
  
  stili = { stili{2:end}, stili{1} };
end

for fig=fig_num
  figure(fig)
  xlabel('f');
  ylabel('|H(f)|');
  title('Risposta in frequenza di filtri ellittici')
  grid on
  
  labels = {};
  for n=ordini
    labels = {labels{:}, sprintf('N=%d', n)};
  end
  
  legend(labels)
end

figure(1)
ylim([1e-3, 1.1]);
print -depsc filtri_ellittici.eps

figure(2)
xlim([0.8, 1.2]);
ylim([1e-1, 1.1]);
print -depsc filtri_ellittici_zoom.eps