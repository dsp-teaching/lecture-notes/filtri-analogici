function [y,L,z]=rational_ellip(order, xi, x)

  if order==1
    [y,L,z]=r1(xi, x);

  elseif order==2
    [y, L, z]=r2(xi, x);

  elseif order==3
    [y, L, z]=r3(xi, x);

  elseif mod(order, 2)==0
    [u, M]=rational_ellip(order / 2, xi, x);
    [y, L]=r2(M, u);
    z=[];
    
  elseif mod(order, 3)==0
    [u, M]=rational_ellip(order / 3, xi, x);
    [y, L]=r3(M, u);
    z=[];
    
  else
    error('Order %d not implemented', order);
  end
endfunction

function [y, L, z]=r1(xi,x)
  y=x;
  L=xi;
  z=0;
endfunction

function [y, L,z]=r2(xi, x)
  t=sqrt(1-1/xi^2);
  u = (t+1)*x.^2;
  v = (t-1)*x.^2;
  y=(u-1) ./ (v+1);
  L=(1+t)/(1-t);

  z = abs(xi)*sqrt(1-t);
endfunction

function [y,L,z]=r3(xi, x)
  u=xi^2;
  
  G=sqrt(4*u + (4*u*(u-1))^(2/3));
  num = 2*u*sqrt(G);
  den = sqrt(8*u*(u+1)+12*G*u-G^3)-sqrt(G^3);
  xp2 = num/den;
  xz2 = u / xp2;

  num_y = x.*(1-xp2).*(x.^2-xz2);
  den_y = (1-xz2)*(x.^2-xp2);

  y = num_y ./ den_y;
  L = xi^3*((1-xp2)/(u-xp2))^2;

  z=[sqrt(xz2), 0];
endfunction
