stampa=0
omega0 = 1;
Q = [0.75, 5, 30, 50, 100, 300, 500, 1000];

fig_num = [1 2 3 4]
ordini = [4 6 8];

stili_start = {'b', 'r--', 'g-.', 'k:'};

f=logspace(0, 1, 5000);

for fig=fig_num
  figure(fig)
  hold off
end

figure(1)
stili = stili_start;
for q=Q
  xi = 1/(2*q);
  coeff = [1/omega0^2, 2*xi/omega0, 1];

  figure(1)
  loglog(f, 1 ./ abs(polyval(coeff, j*f/(2*pi))), stili{1});
  hold on
  
  figure(2)
  s = stili{1};
  h=plot(roots(coeff), [s(1) '*']);
  set(h, 'markersize', 10)
  hold on
  
  stili = { stili{2:end}, stili{1} };
end

figure(2)
stili = stili_start;
last_root = [0,0];
for omega=[0.33, 1, 3]
  xi = 1/(2*q);
  coeff = [1/omega^2, 2*xi/omega, 1];
  
  figure(3)
  loglog(f, 1 ./ abs(polyval(coeff, j*f/(2*pi))), stili{1});
  hold on
  
  figure(4)
  s = stili{1};
  rad=roots(coeff);
  h=plot(rad, [s(1) '*']);
  set(h, 'markersize', 10)
  hold on
  if (real(rad(1)) < last_root(1)) 
    last_root = [real(rad(1)), imag(rad(1))];
  end
  
  stili = { stili{2:end}, stili{1} };
end

figure(2)
plot(exp(j*pi*(0.5:0.01:1.5)), '--')
% xlim(1.2*xlim+0.1)
% ylim(1.2*ylim)
axis equal
title('Posizione dei poli per filtri a \omega_0 costante')
xlabel('Re(p)')
ylabel('Im(p)')

figure(4)
xlim(1.2*xlim)
ylim(1.2*ylim)
line([0, last_root(1)], [0, last_root(2)], 'linestyle', '--')
line([0, last_root(1)], [0, -last_root(2)], 'linestyle', '--')
title('Posizione dei poli per filtri a Q costante')
xlabel('Re(p)')
ylabel('Im(p)')

figure(1)
xlabel('f')
ylabel('|H(f)|')
title('Risposta in frequenza di filtri a \omega_0 costante')

figure(3)
xlabel('f')
ylabel('|H(f)|')
title('Risposta in frequenza di filtri a Q costante')

for fig=fig_num
  figure(fig)
%  grid on
end

if stampa
  figure(1)
  print -depsc filtri_omega_costante.eps

  figure(2)
  print -depsc poli_omega_costante.eps

  figure(3)
  print -depsc filtri_q_costante.eps

  figure(4)
  print -depsc poli_q_costante.eps

end