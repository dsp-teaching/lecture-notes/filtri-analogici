syms Z1 Z2 Z3 Z4 Z5 
syms R1 R2 Rf C1 Cf s

M = [   1,  -1,   0,  Z1,   0,   0,   0
	0,   1,   0,   0,   0, -Z3,   0
	0,   0,  -1,   0,   0, -Z5,   0
	0,   1,  -1,   0,   0,   0, -Z4
	0,  -1,   0,   0,  Z2,   0,   0
	0,   0,   0,  -1,   1,   1,   1];

v = null(M);
H = v(3)/v(1);

old=[Z1 Z2 Z3 Z4 Z5];
new=[R1, 1/(s*C1), R2, Rf, 1/(s*Cf)];

H_lp = collect(simplify(subs(H, old, new)));

pretty(H_lp)

H_lp_simple=collect(simplify(subs(H_lp, Rf, R1)));

pretty(H_lp_simple)

syms G
H_lp_G=collect(simplify(subs(H_lp, Rf, R1*G)));

pretty(H_lp_G)
